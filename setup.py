#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

requirements = setup_requirements = test_requirements = [
    "lxml",
]

setup(
    author="Zlatko Masek",
    author_email='zlatko.masek@gmail.com',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: The MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
    ],
    description="Convert Trello JSON export to MM file for Coogle.",
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description="Convert Trello JSON export to MM file for Coogle.",
    include_package_data=True,
    keywords='mellow',
    name='mellow',
    packages=find_packages(include=['mellow']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://bitbucket.org/zmasek/mellow',
    version='0.1.1',
    zip_safe=False,
)

