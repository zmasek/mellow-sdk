"""Convert input Trello json to output MM file for Coggle
Example: python mellow.py trello.json coggle.mm
or just import mellow in another project.
"""
import hashlib
import json
import sys

from lxml import etree

POSITIONS = ['left', 'right']


def _main(file_input, file_output):
    """The main entrypoint for calling the file directly.

    :param file_input: Path to Trello JSON file.
    :type file_input: str.
    :param file_output: Path where the script will output the results.
    :type file_output: str.
    """
    with open(file_input, 'r') as trello_file:
        trello = json.load(trello_file)

    xml = mellow(trello)

    xml.write(file_output, pretty_print=True)


def mellow(trello):
    """Converts the input dict to ElementTree instance.

    :param trello: Python representation of Trello JSON.
    :type trello: dict.
    :returns:  ElementTree instance.
    """
    root = etree.Element('map')
    root.set('version', '0.9.0')
    board = _create_node(root, trello, 50)
    board.attrib.update({
        'X_COGGLE_POSX': '0',
        'X_COGGLE_POSY': '0',
    })

    counter = 0
    for list_trello in trello['lists']:
        if not list_trello['closed']:
            position = _get_position(counter)
            counter += 1
            list_mind = _create_node(board, list_trello, 31, position)
            for card_trello in trello['cards']:
                if card_trello['idList'] == list_trello['id'] and not card_trello['closed']:
                    card_mind = _create_node(list_mind, card_trello, 13, position)
                    _augment_node(card_mind, card_trello)
                    for checklist_trello in trello['checklists']:
                        if checklist_trello['idCard'] == card_trello['id']:
                            checklist_mind = _create_node(card_mind, checklist_trello, 8, position)
                            _create_checklist(checklist_mind, checklist_trello)

    return etree.ElementTree(root)


def _augment_node(node, source):
    """Add description and pictures from source dictionary to the provided node.

    :param node: Which SubElement to change.
    :type node: SubElement instance.
    :param source: Segment of Trello JSON dictionary.
    :type source: dict.
    """
    if source['desc']:
        node.set('TEXT', '*{}*\n\n{}'.format(node.get('TEXT'), source['desc']))
    for attachment in source['attachments']:
        picture = '![{}]({})'.format(attachment['name'], attachment['url'])
        node.set('TEXT', '{}\n\n{}'.format(node.get('TEXT'), picture))


def _create_checklist(node, source):
    """Add checklist in markdown format from source dictionary to the provided node.

    :param node: Which SubElement to change.
    :type node: SubElement instance.
    :param source: Segment of Trello JSON dictionary.
    :type source: dict.
    """
    markdown = '*{}*\n\n'.format(node.get('TEXT'))
    for cit in source['checkItems']:
        markdown += '- [{}] {}\n'.format(' ' if cit['state'] == 'incomplete' else 'X', cit['name'])
    node.set('TEXT', markdown)


def _get_position(counter):
    """Get position where to add the text on the diagram.

    :param counter: Which loop pass the script is in.
    :type counter: int.
    :returns: Position string from POSITIONS constant.
    """
    index = counter % len(POSITIONS)
    return POSITIONS[index-1]


def _create_presentation(node, size):
    """Add color, font name and font size to the provided node.

    :param node: Which SubElement to change.
    :type node: SubElement instance.
    :param size: What size for the text to use.
    :type size: int.
    """
    edge = etree.SubElement(node, 'edge')
    color = '#{}'.format(hashlib.sha224(node.get('TEXT').encode('utf-8')).hexdigest()[:6])
    edge.set('COLOR', color)
    font = etree.SubElement(node, 'font')
    font.attrib.update({
        'NAME': 'Helvetica',
        'SIZE': str(size),
    })


def _create_node(element, source, size, position='right'):
    """Create SubElement for the ElementTree.

    :param element: Which ElementTree node to attach the new node to.
    :type element: ElementTree node.
    :param source: Segment of Trello JSON dictionary.
    :type source: dict.
    :param size: What size for the node text to use.
    :type size: int.
    :param position: What position to put the text to.
    :type position: str.
    """
    node = etree.SubElement(element, 'node')
    node.attrib.update({
        'TEXT': source['name'],
        'FOLDED': 'false',
        'POSITION': position,
        'ID': source['id'],
    })
    _create_presentation(node, size)
    return node


if __name__ == '__main__':
    _main(sys.argv[1], sys.argv[2])
