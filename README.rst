==============================
Trello board to Coggle diagram
==============================

Export Trello board in settings to JSON, then save it as a file and pass to the Mellow function in the module. As an output, you get an ElementTree that you can save to disk and then drag and drop in a Coggle new diagram.

If you want to use it as a service, visit `Mellow <https://mellow.offsetlab.net>`_ web. The service is made with two separate repositories serving from AWS Lambda with `mellow-faas <https://bitbucket.org/zmasek/mellow-faas/>`_ and Netlify with `mellow-fe <https://github.com/zmasek/mellow-fe/>`_.
